const { generateFileLink } = require('../../utils/fileUpload');
const response = require('../../utils/response');
const productService = require('./product.service');

exports.getProduct = async (req, res) => {
    const { pageSize, pageNumber } = req.query;

    const result = await productService.fetchProduct({ pageSize, pageNumber });
    if (!result.error) {
        return response.ok(res, result);
    } else {
        return response.noData(res, result);
    }
}

exports.addProduct = async (req, res) => {
    const { name, categoryId } = req.body;
    let { id: createdBy } = req.userData;
    const productData = { name, images : req.files.images || [], createdBy, categoryId };
    const result = await productService.insertProduct(productData);
    if (!result.error) {
        return response.created(res, result);
    } else {
        return response.unprocessableEntity(res, result);
    }
}

exports.updateProductById = async (req, res) => {
    const { id : productId } = req.params;
    const { name, categoryId } = req.body;
    let { id: updatedBy } = req.userData;
    const productData = { name, images : req.files.images || [], productId, updatedBy, categoryId };   
    const result = await productService.updateProduct(productData);
    if (!result.error) {
        return response.ok(res, result);
    } else {
        return response.noData(res, result);
    }
}

exports.removeProductById = async (req, res) => {
    const { id } = req.params;
    const result = await productService.deleteProductById(id);
    if (!result.error) {
        return response.ok(res, result);
    } else {
        return response.noData(res, result);
    }
}

exports.insertproducts = async (req, res) => {
    try {
        const result = await productService.bulkInsert();
        if (!result.error) {
            return response.created(res, result);
        } else {
            return response.unprocessableEntity(res, result);
        }
    } catch (error) {
        console.error('An error occurred:', error);
        return response.noData(res, error);
    }
};
