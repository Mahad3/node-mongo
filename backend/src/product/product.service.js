const { db } = require('../../models')
const { pagination, getFromAndToFromPageData } = require('../../utils/pagination');
const { generateFileLink } = require('../../utils/fileUpload');
const { insertImages } = require('../image/image.service');

exports.fetchProduct = async (query) => {
  // const { skip, limit } = pagination(query.skip, query.limit);
  let {pageNumber, pageSize} = query;
  const {from, to} = getFromAndToFromPageData(pageSize,pageNumber)

  const result = await db.product.find()
  .populate({path : "images", select:"url"})
  .populate({path : "createdBy", select:"userName email"})
  .populate({path : "updatedBy", select:"userName email"})
  .populate({path : "category", select:"name"})
  .sort({ createdAt: -1 })
  .skip(from).limit(to);
  // const result2 = await db.user.find().populate("products").lean();//.sort({ updatedAt: -1 }).skip(skip).limit(limit);
  
  const count = await db.product.count();
  if (result && result.length) {
    return { error: null, message: "Data Found", data: result, count: count }
  } else {
    return { error: true, message: "Data Not Found", data: result, count: count }
  }
}

exports.insertProduct = async (productData) => {
  const { name, images, createdBy, categoryId } = productData;
  let imgLink  = images.map((e)=>({url : generateFileLink(e.path)}));
  let {data : imageIds} = await insertImages(imgLink);

  const result = await db.product.create({ name, createdBy, images: imageIds, category: categoryId });
  if (result) {
    return { error: null, message: "Data Inserted", data: result };
  } else {
    return { error: true, message: "Error while inserting data", data: [] };
  }
}

exports.updateProduct = async (updatedProductData) => {
  const { name, images, productId, updatedBy, categoryId } = updatedProductData;

  let imgLink = images.map((e) => ({ url: generateFileLink(e.path) }));

  let { data: newImageIds } = await insertImages(imgLink);

  const existingProduct = await db.product.findById(productId);

  if (categoryId) {
    let category = await db.category.findById(categoryId);
    existingProduct.category = category ? categoryId : existingProduct.category;
  }

  existingProduct.name = name ? name : existingProduct.name;
  existingProduct.updatedBy = updatedBy;
  
  // console.log(existingProduct.images)
  existingProduct.images = [...existingProduct.images, ...newImageIds];

  const updatedProduct = await existingProduct.save();

  if (updatedProduct) {
    return { error: null, message: "Data Updated", data: updatedProduct };
  } else {
    return { error: true, message: "Error while updating data" };
  }
};


exports.deleteProductById = async (id) => {
  const result = await db.product.deleteOne({ _id: id })
  if (result) {
    return { error: null, data: result, message: "Product Deleted" };
  } else {
    return { error: true, data: result, message: "Product Not found" };
  }
}

