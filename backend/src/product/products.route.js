const express = require('express');

const router = express.Router();
const productController = require('./product.controller');
const { errorWrapper } = require('../../utils/error');
const checkAuth = require('../../middleware/checkAuth');
const { upload } = require('../../utils/fileUpload');

router.post('/', checkAuth, upload, errorWrapper(productController.addProduct));
router.get('/', checkAuth, errorWrapper(productController.getProduct));
router.put('/:id', checkAuth, upload,errorWrapper(productController.updateProductById));
router.delete('/:id', checkAuth, errorWrapper(productController.removeProductById));

module.exports = router;
