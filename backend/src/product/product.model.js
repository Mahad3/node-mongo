const mongoose = require('mongoose');

const productSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    category: { type: mongoose.Schema.Types.ObjectId, ref: 'category', required: true},
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'user', required: true },
    updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'user', required: false },
    images : [{ type: mongoose.Schema.Types.ObjectId, ref: 'image' }]
  },
  { timestamps: true },
);

const product = mongoose.model('product', productSchema);

module.exports = product;
