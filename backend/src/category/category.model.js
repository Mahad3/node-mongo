const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'user', required: true },
    updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'user', required: false },
    images : [{ type: mongoose.Schema.Types.ObjectId, ref: 'image' }]
  },
  { timestamps: true },
);

const category = mongoose.model('category', categorySchema);

module.exports = category;
