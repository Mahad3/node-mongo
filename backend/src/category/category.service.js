const { db } = require('../../models')
const { pagination, getFromAndToFromPageData } = require('../../utils/pagination');
const { generateFileLink } = require('../../utils/fileUpload');
const { insertImages } = require('../image/image.service');

exports.fetchCategory = async (query) => {
  // const { skip, limit } = pagination(query.skip, query.limit);
  let {pageNumber, pageSize, search} = query;
  // search = search == undefined ? "" : search;
  const {from, to} = getFromAndToFromPageData(pageSize,pageNumber);
  // const result = await db.category.find()
  //   .populate({ path: "images", select: "url" })
  //   .populate({ path: "createdBy", select: "userName email" })
  //   .populate({ path: "updatedBy", select: "userName email" })
  //   .populate({ path: "categories", select: "name", model: "products", strictPopulate: false })
  //   .sort({ updatedAt: -1 })
  //   .skip(skip).limit(limit);
  const result = await db.category.aggregate([
    {
      $lookup: {
        from: 'products',
        localField: '_id',
        foreignField: 'category',
        as: 'products'
      }
    },
    {
      $lookup: {
        from: 'users',
        localField: 'createdBy',
        foreignField: '_id',
        as: 'createdBy'
      }
    },
    {
      $unwind: {
        path: "$createdBy",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'users',
        localField: 'updatedBy',
        foreignField: '_id',
        as: 'updatedBy'
      }
    },
    {
      $unwind: {
        path: "$updatedBy",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'images',
        localField: 'images',
        foreignField: '_id',
        as: 'images'
      }
    },
    {
      $project: {
        _id: 1,
        name: 1,
        createdAt: 1,
        products: {
          _id: 1,
          name: 1,
        },
        createdBy: {
          _id: 1,
          userName: 1,
          email: 1,
        },
        updatedBy: {
          _id: 1,
          userName: 1,
          email: 1,
        },
        images: {
          $map: {
            input: '$images',
            as: 'image',
            in: {
              _id: '$$image._id',
              userName: '$$image.url',
            }
          }
        },
      }
    },
    // (search && { $match : { name : search } } ),
    { $sort : { createdAt : -1} },
    {
      $skip: from
    },
    {
      $limit: to
    },
  ]).exec();

  const count = await db.category.count();
  if (result && result.length) {
    return { error: null, message: "Data Found", data: result, count: count }
  } else {
    return { error: true, message: "Data Not Found", data: result, count: count }
  }
}

exports.insertCategory = async (categoryData) => {
  const { name, images, createdBy } = categoryData;
  let imgLink = images.map((e) => ({ url: generateFileLink(e.path) }));
  let { data: imageIds } = await insertImages(imgLink);

  const result = await db.category.create({ name, createdBy, images: imageIds });
  if (result) {
    return { error: null, message: "Data Inserted", data: result };
  } else {
    return { error: true, message: "Error while inserting data", data: [] };
  }
}

exports.updateCategory = async (updatedCategoryData) => {
  const { name, images, categoryId, updatedBy } = updatedCategoryData;

  let imgLink = images.map((e) => ({ url: generateFileLink(e.path) }));

  let { data: newImageIds } = await insertImages(imgLink);

  const existingCategory = await db.category.findById(categoryId);

  existingCategory.name = name;
  existingCategory.updatedBy = updatedBy;
  // console.log(existingCategory.images)
  existingCategory.images = [...existingCategory.images, ...newImageIds];

  const updatedCategory = await existingCategory.save();

  if (updatedCategory) {
    return { error: null, message: "Data Updated", data: updatedCategory };
  } else {
    return { error: true, message: "Error while updating data" };
  }
};


exports.deleteCategoryById = async (id) => {
  const result = await db.category.deleteOne({ _id: id })
  if (result) {
    return { error: null, data: result, message: "Category Deleted" };
  } else {
    return { error: true, data: result, message: "Category Not found" };
  }
}

