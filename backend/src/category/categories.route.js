const express = require('express');

const router = express.Router();
const categoryController = require('./category.controller');
const { errorWrapper } = require('../../utils/error');
const checkAuth = require('../../middleware/checkAuth');
const { upload } = require('../../utils/fileUpload');

router.post('/', checkAuth, upload, errorWrapper(categoryController.addCategory));
router.get('/', checkAuth, errorWrapper(categoryController.getCategory));
router.put('/:id', checkAuth, upload,errorWrapper(categoryController.updateCategoryById));
router.delete('/:id', checkAuth, errorWrapper(categoryController.removeCategoryById));

module.exports = router;
