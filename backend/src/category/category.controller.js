const { generateFileLink } = require('../../utils/fileUpload');
const response = require('../../utils/response');
const categoryService = require('./category.service');

exports.getCategory = async (req, res) => {
    const { pageNumber, pageSize, search } = req.query;

    const result = await categoryService.fetchCategory({ pageNumber, pageSize, search });
    if (!result.error) {
        return response.ok(res, result);
    } else {
        return response.noData(res, result);
    }
}

exports.addCategory = async (req, res) => {
    const { name } = req.body;
    let { id: createdBy } = req.userData;
    const categoryData = { name, images : req.files.images || [], createdBy };
    const result = await categoryService.insertCategory(categoryData);
    if (!result.error) {
        return response.created(res, result);
    } else {
        return response.unprocessableEntity(res, result);
    }
}

exports.updateCategoryById = async (req, res) => {
    const { id : categoryId } = req.params;
    const { name } = req.body;
    let { id: updatedBy } = req.userData;
    const categoryData = { name, images : req.files.images || [], categoryId, updatedBy };   
    const result = await categoryService.updateCategory(categoryData);
    if (!result.error) {
        return response.ok(res, result);
    } else {
        return response.noData(res, result);
    }
}

exports.removeCategoryById = async (req, res) => {
    const { id } = req.params;
    const result = await categoryService.deleteCategoryById(id);
    if (!result.error) {
        return response.ok(res, result);
    } else {
        return response.noData(res, result);
    }
}

exports.insertcategorys = async (req, res) => {
    try {
        const result = await categoryService.bulkInsert();
        if (!result.error) {
            return response.created(res, result);
        } else {
            return response.unprocessableEntity(res, result);
        }
    } catch (error) {
        console.error('An error occurred:', error);
        return response.noData(res, error);
    }
};
