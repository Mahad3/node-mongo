const response = require('../../utils/response');
const { db } = require('../../models');

exports.getUser = async (req, res) => {
    const { skip, limit } = req.query;

    const result = await db.user.aggregate([
        {
            $lookup: {
                from: 'products',
                localField: '_id',
                foreignField: 'createdBy',
                as: 'products'
            }
        },
        {
            $lookup: {
                from: 'categories',
                localField: '_id',
                foreignField: 'createdBy',
                as: 'categories'
            }
        },
        {
            $project: {
                _id: 1,
                userName: 1,
                email: 1,
                categories: {
                    _id: 1,
                    name: 1,
                },
                products: {
                    _id: 1,
                    name: 1,
                },
                __v: 1
            }
        }
    ]).exec();

    if (!result.error) {
        return response.ok(res, result);
    } else {
        return response.noData(res, result);
    }
}


