const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
    {
        userName: {
            type: String,
            required: true,
            unique: true
        },
        email: {
            type: String,
            required: true,
            unique: true
        },
        password: {
            type: String,
            required: true
        },
        // posts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'post' }],
    },
    { timestamps: true },
);

const user = mongoose.model('user', userSchema);

module.exports = user;

