const { db } = require('../../models')

exports.insertImages = async (imageLinkArray) => {
  const inserted = await db.image.insertMany(imageLinkArray);
  if (inserted) {
    let result = inserted.map((ele)=>ele.id);
    return { error: null, message: "Data Inserted", data: result };
  } else {
    return { error: true, message: "Error while inserting data", data: null };
  }
}
