const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema(
  {
    url: { type: String, required: true },
  },
  { timestamps: true },
);

const category = mongoose.model('image', categorySchema);

module.exports = category;
