/**
* @swagger
* /categories:
*   get:
*     tags:
*       - Category
*     summary: Filter and Search Category
*     security:
*       - bearerAuth: []
*     parameters:
*       - name: pageSize
*         in: query
*         required: false
*         description: size of page
*         type: integer
*       - name: pageNumber
*         in: query
*         required: false
*         description: page number
*         type: integer
*     responses:
*       200:
*         description: List of categories filtered and/or searched
*         schema:
*           type: array
*       422:
*         description: No categories found for the specified filters or keywords
*         schema:
*           type: object
*           properties:
*             message:
*               title: message
*               type: string
*               example: No categories found for the specified filters or keywords
*   post:
*     tags:
*       - Category
*     summary: Add Category
*     security:
*       - bearerAuth: []
*     consumes:
*       - multipart/form-data
*     parameters:
*       - in: formData
*         name: images
*         type: array
*         items:
*           type: file
*       - in: formData
*         name: name
*         type: string
*         example: Pc
*     responses:
*       201:
*         description: created
*         schema:
*           type: object
*           properties:
*             message:
*               title: message
*               type: string
*               example: success
*       422:
*         description: Failed
*         schema:
*           type: object
*           description: upload failed
*           properties:
*             message:
*               title: message
*               type: string
*               example: failed
* /categories/{id}:
*   put:
*     tags:
*       - Category
*     summary: Update Category
*     security:
*       - bearerAuth: []
*     consumes:
*       - multipart/form-data
*     parameters:
*       - in: formData
*         name: images
*         type: array
*         items:
*           type: file
*       - in: formData
*         name: name
*         type: string
*         example: Updated Pc
*       - in: path
*         name: id
*         type: string
*         required: true
*         description: ID of the category to be updated
*     responses:
*       200:
*         description: Category updated successfully
*         schema:
*           type: object
*           properties:
*             message:
*               title: message
*               type: string
*               example: success
*       404:
*         description: Category not found
*         schema:
*           type: object
*           properties:
*             message:
*               title: message
*               type: string
*               example: Category not found
*       422:
*         description: Failed to update category
*         schema:
*           type: object
*           description: Category
*           properties:
*             message:
*               title: message
*               type: string
*               example: failed to update category
*   delete:
*     tags:
*       - Category
*     summary: To get delete by id
*     parameters:
*     - name: "id"
*       in: "path"
*       description: "Id of categories to delete"
*       required: true
*       type: "string"
*     security:
*       - bearerAuth: []
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: To delete categories by id.
*         schema:
*           type: object
*           properties:
*       404:
*         description: Product
*         schema:
*           type: object
*           description: not found
*           properties:
*             message:
*               title: message
*               type: string
*               example: not found
*/


// *       - name: search
// *         in: query
// *         required: false
// *         description: Filter categories by category
// *         type: string
