/**
* @swagger
* /products:
*   get:
*     tags:
*       - Product
*     summary: Filter and Search Product
*     security:
*       - bearerAuth: []
*     parameters:
*       - name: pageSize
*         in: query
*         required: false
*         description: page size
*         type: integer
*       - name: pageNumber
*         in: query
*         required: false
*         description: page size
*         type: integer
*     responses:
*       200:
*         description: List of products filtered and/or searched
*         schema:
*           type: array
*       422:
*         description: No products found for the specified filters or keywords
*         schema:
*           type: object
*           properties:
*             message:
*               title: message
*               type: string
*               example: No products found for the specified filters or keywords
*   post:
*     tags:
*       - Product
*     summary: Add Product
*     security:
*       - bearerAuth: []
*     consumes:
*       - multipart/form-data
*     parameters:
*       - in: formData
*         name: categoryId
*         type: string
*         example: 31926492187
*       - in: formData
*         name: images
*         type: array
*         items:
*           type: file
*       - in: formData
*         name: name
*         type: string
*         example: Pc
*     responses:
*       201:
*         description: created
*         schema:
*           type: object
*           properties:
*             message:
*               title: message
*               type: string
*               example: success
*       422:
*         description: Failed
*         schema:
*           type: object
*           description: upload failed
*           properties:
*             message:
*               title: message
*               type: string
*               example: failed
* /products/{id}:
*   put:
*     tags:
*       - Product
*     summary: Update Product
*     security:
*       - bearerAuth: []
*     consumes:
*       - multipart/form-data
*     parameters:
*       - in: formData
*         name: categoryId
*         type: string
*         example: 31926492187
*       - in: formData
*         name: images
*         type: array
*         items:
*           type: file
*       - in: formData
*         name: name
*         type: string
*         example: Updated Pc
*       - in: path
*         name: id
*         type: string
*         required: true
*         description: ID of the product to be updated
*     responses:
*       200:
*         description: Product updated successfully
*         schema:
*           type: object
*           properties:
*             message:
*               title: message
*               type: string
*               example: success
*       404:
*         description: Product not found
*         schema:
*           type: object
*           properties:
*             message:
*               title: message
*               type: string
*               example: Product not found
*       422:
*         description: Failed to update product
*         schema:
*           type: object
*           description: Product
*           properties:
*             message:
*               title: message
*               type: string
*               example: failed to update product
*   delete:
*     tags:
*       - Product
*     summary: To get delete by id
*     parameters:
*     - name: "id"
*       in: "path"
*       description: "Id of products to delete"
*       required: true
*       type: "string"
*     security:
*       - bearerAuth: []
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: To delete products by id.
*         schema:
*           type: object
*           properties:
*       404:
*         description: Product
*         schema:
*           type: object
*           description: not found
*           properties:
*             message:
*               title: message
*               type: string
*               example: not found
*/
