const multer = require("multer");
const path = require("path");
// Set the storage engine
const storage = multer.diskStorage({
    destination(req, file, cb) {
        let dest = "public/uploads/";
        cb(null, dest);
    },
    filename(req, file, cb) {
        let fileName = `${file.fieldname}-${Date.now()}${path.extname(
            file.originalname
        )}`;
        cb(null, fileName);
    },
});

// Initialize the upload

function checkFileType(file, cb) {
    const allowedFileTypes = [".jpg", ".jpeg", ".png"];
    // Check if the file extension is in the allowed file types
    const fileExtension = `.${file.originalname.split(".").pop()}`;
    if (allowedFileTypes.includes(fileExtension)) {
        cb(null, true);
    } else {
        cb(new Error(`File should be an image!`));
    }
}

exports.upload = multer({
    storage,
    limits: { fileSize: 100 * 1024 * 1024 }, // 100MB
    fileFilter(req, file, cb) {
        checkFileType(file, cb);
    },
}).fields([
    { name: "images", maxCount: 8 },
]);

exports.generateFileLink = (path) => {
    if (!path || typeof path != 'string') {
      return null
    }
    path = path.replaceAll('\\', '/');
    path = path.replace('./public/', '')
    path = path.replace('public/', '')
    return `${process.env.BASE_URL}/${path}`
  }
  
