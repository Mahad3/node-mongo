exports.pagination = (skipDoc, limitDoc) => {
    const skip = Number(skipDoc) >= 0 ? parseInt(skipDoc, 10) : 0;
    const limit = Number(limitDoc) >= 10 ? parseInt(limitDoc, 10) : 10;
    return { skip, limit };
  };

exports.getFromAndToFromPageData = (pageSize, pageNumber)=>{
    const defaultPageSize = 10; 
    const defaultPageNumber = 1; 
  
    const size = parseInt(pageSize) || defaultPageSize;
    const number = parseInt(pageNumber) || defaultPageNumber;
  
    const from = (number - 1) * size;
    const to = number * size;
  
    return { from, to };
  
}